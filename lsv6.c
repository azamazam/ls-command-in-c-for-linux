#include <unistd.h>
#include <stdio.h>
#include <dirent.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <pwd.h>
#include<grp.h> 
#include <string.h>
#include <sys/ioctl.h>
#include <ctype.h>


extern int  errono;
void  do_ls_ls(char*);
void do_ls(char*);
void  show_file_info(char[],char*);
char* modePermissionConversion(int );
int cstring_cmp(const void *, const void *); 
int  getcolumns();
void printFileNames(char *,char*);

int  main (int  argc,char*  argv[])
{   
	if  (argc==1)
	{
	
		do_ls(".");
		return (0);
	}
	else if  (strcmp(argv[1],"-ls")==0){
		if  (argc==2)
		{
	
			do_ls_ls(".");
			return (0);
		}	
		int  i=1;
	 	while (++i<argc)
	 	{
	 		printf ("DIrectory listing of %s :\n",argv[i]);
	 		do_ls_ls(argv[i]);
	 	
	 	}
	 	return  0;
    }
    else {
    int  i=0;
	 	while (++i<argc)
	 	{
	 		printf ("DIrectory listing of %s :\n",argv[i]);
	 		do_ls(argv[i]);
	 	
	 	}
	 	return  0;
    }
    
}

void  do_ls_ls(char* dir)
{
	struct  dirent *entry ;

    DIR* dp=opendir(dir);  
  	if  (dp==NULL)
  	{
	  	fprintf(stderr,"Cant opened  directory : %s\n",dir);
	  	return;
  	}
    errno=0;  
	int  noOfFiles=0;
    while (1){
        entry =readdir(dp);
        if (entry==NULL && errno !=0){
            perror("readDir");
            return ;
        }
        else if (entry==NULL && errno ==0){ 
            break;
        }
        if (entry->d_name[0]=='.'   )
		continue;
		noOfFiles++;
			
	}
  
    
    char **names=(char**)malloc(sizeof(char**)*noOfFiles);
    
    dp=opendir(dir);  
  	if  (dp==NULL)
  	{
	  	fprintf(stderr,"Cant opent  directory : %s\n",dir);
	  	return;
  	}
    errno=0;  
 	int  i=0;
    while (1){
        entry =readdir(dp);
        if (entry==NULL && errno !=0){
            perror("readDir");
            return ;
        }
        else if (entry==NULL && errno ==0){
         break;       
        }
        if (entry->d_name[0]=='.'   )
		continue;
	
		names[i] = (char*)malloc(sizeof(char) * (strlen(entry->d_name) + 1 ) );
		strcpy(names[i], entry->d_name);
//		printf("%s\n",names[i]);
		i++;
     
    }
    closedir(dp);
    qsort(names, noOfFiles, sizeof(char *), cstring_cmp);	
	for(int i=0; i<noOfFiles; i++)
	{						
		char path[1000];
		strcpy(path,dir);     //this  is  necessary  for  other  directries 
		strcat(path,"/");
		strcat(path,names[i]);
		show_file_info(path,names[i]);
	}
}

void  show_file_info(char *path,char *fname)
{
	struct stat info;
	int  rv=lstat(path,&info);
	if (rv==-1){
		perror("stat failed ");
		exit(1);
	}
	
	struct passwd* pwd=getpwuid(info.st_uid);
	struct group* grp=getgrgid(info.st_gid);
	struct tm tmStruct;
	localtime_r(&info.st_mtime, &tmStruct);
	char time_str[64];
	strftime (time_str, sizeof (time_str), "%b %e %H:%M",&tmStruct); 
	char* mode=modePermissionConversion(info.st_mode);
	
	printf("%s\t",mode);
	printf("%ld ",info.st_nlink);
	printf("%s\t",pwd->pw_name);
	printf("%s\t",grp->gr_name);
	printf("%ld\t",info.st_size);
	printf("%s\t",time_str);
	printFileNames(mode,fname);
	

	
}
char* modePermissionConversion(int mode){
char *str=malloc(sizeof(char)*10);
	strcpy(str,"----------");
	if((mode & 0170000) == 0010000)
		str[0]='p';
	else if((mode & 0170000) == 0020000)
		str[0]='c';
	else if((mode & 0170000) == 0040000)
		str[0]='d';
	else if((mode & 0170000) == 0060000)
		str[0]='b';
	else if((mode & 0170000) == 0100000)
		str[0]='-';
	else if((mode & 0170000) == 0120000)
		str[0]='l';
	else if((mode & 0170000) == 0140000)
		str[0]='s';
	else 
		str[0]='~';		//unknown
	if((mode & 0000400) == 0000400)
		str[1]='r';
	if((mode & 0000200) == 0000200)
		str[2]='w';
	if((mode & 0000100) == 0000100)
		str[3]='x';
	if((mode & 0000040) == 0000040)
		str[4]='r';
	if((mode & 0000020) == 0000020)
		str[5]='w';
	if((mode & 0000010) == 0000010)
		str[6]='x';
	if((mode & 0000004) == 0000004)
		str[7]='r';
	if((mode & 0000002) == 0000002)
		str[8]='w';
	if((mode & 0000001) == 0000001)
		str[9]='x';
	if((mode & 0004000) == 0004000)
		str[3]='s';
	if((mode & 0002000) == 0002000)
		str[6]='s';
	if((mode & 0001000) == 0001000)
		str[9]='t';	
	//printf("%s\n",str);
	return str;
}


int cstring_cmp(const void *a, const void *b) 
{ 
    const char **ia = (const char **)a;
    const char **ib = (const char **)b;
    return strcmp(*ia, *ib);
	/* strcmp functions works exactly as expected from
	comparison function */ 
} 

void  do_ls(char* dir)
{
	int col=getcolumns();
	int  maxlen=0;
	int  noOfFiles=0;
	struct  dirent *entry ;

    DIR* dp=opendir(dir);  
  	if  (dp==NULL)
  	{
	  	fprintf(stderr,"Cant opent  directory : %s\n",dir);
	  	return;
  	}
    errno=0;  
    
    
   
    while (1){
        entry =readdir(dp);
        if (entry==NULL && errno !=0){
            perror("readDir");
            return ;
        }
        else if (entry==NULL && errno ==0){ 
            break ;
        }
        if (entry->d_name[0]=='.')
		continue;
	
	//	printf("%-15s",entry->d_name);	
		noOfFiles++;
        int l=strlen(entry->d_name);
     
        if  (maxlen<l) maxlen=l;
     
    
        
    }
   
    char **names=(char**)malloc(sizeof(char**)*noOfFiles);
    dp=opendir(dir);  
  	if  (dp==NULL)
  	{
	  	fprintf(stderr,"Cant opent  directory : %s\n",dir);
	  	return;
  	}
    errno=0;  
    
    
   int  i=0;
    while (1){
        entry =readdir(dp);
        if (entry==NULL && errno !=0){
            perror("readDir");
            return ;
        }
        else if (entry==NULL && errno ==0){ 
            break ;
        }
        if (entry->d_name[0]=='.')
		continue;
	
        names[i] = (char*)malloc(sizeof(char) * (strlen(entry->d_name) + 1 ) );
		strcpy(names[i], entry->d_name);
		i++;
	}
    qsort(names, noOfFiles, sizeof(char *), cstring_cmp);	
	for( i=0; i<noOfFiles; i++)
	{						
		
		printf("%-10s",names[i]);
	}
    printf("\n");
    closedir(dp);
}


int  getcolumns(){

   struct winsize w;
    ioctl(0, TIOCGWINSZ, &w);

    return w.ws_col;
   
}

void printFileNames(char *mode,char *fname)
{
	if(mode[0]=='d')
		printf("\033[34m\033[1m%s\033[39m\033[m\n",fname);
	else if(mode[0]=='l')
		printf("\033[36m\033[1m%s\033[39m\033[m\n",fname);
	else if(mode[0]=='p')
		printf("\033[33m\033[40m%s\033[39m\033[49m\n",fname);
	else if(mode[0]=='c')
		printf("\033[33m\033[1m\033[40m%s\033[39m\033[m\033[49m\n",fname);
	else if(mode[0]=='b')
		printf("\033[33m\033[1m\033[40m%s\033[39m\033[m\033[49m\n",fname);
	else if(mode[0]=='s')
		printf("\033[35m\033[1m%s\033[39m\033[m\n",fname);
	else if(strstr(fname,".out"))
		printf("\033[32m\033[1m%s\033[39m\033[m\n",fname);
	else if(strstr(fname,".tar") || strstr(fname,".tar.gz") )
		printf("\033[31m\033[1m%s\033[39m\033[m\n",fname);
	else
		printf("%s\n",fname);
	return;
}
