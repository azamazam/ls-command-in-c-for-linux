#include <unistd.h>
#include <stdio.h>
#include <dirent.h>
#include <stdlib.h>
#include <errno.h>

extern int  errono;
void  do_ls(char*);
int  main (int  argc,char*  argv[])
{   if  (argc==1)
	{
	
		do_ls(".");
		return (0);
	}
	else {
	 	printf("Directory listng of %s : \n",argv[1]);
	 	do_ls(argv[1]);
	 	return  0;
    }
}

void  do_ls(char* dir)
{
	struct  dirent *entry ;

    DIR* dp=opendir(dir);  
  	if  (dp==NULL)
  	{
	  	fprintf(stderr,"Cant opent  directory : %s\n",dir);
	  	return;
  	}
    errno=0;  
    while (1){
        entry =readdir(dp);
        if (entry==NULL && errno !=0){
            perror("readDir");
            return ;
        }
        else if (entry==NULL && errno ==0){
            return ;
        }
        else 
        printf("%s\n",entry->d_name);
    }
    closedir(dp);
}
