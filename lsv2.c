#include <unistd.h>
#include <stdio.h>
#include <dirent.h>
#include <stdlib.h>
#include <errno.h>

extern int  errono;
void  do_ls(char*);
int  main (int  argc,char*  argv[])
{   if  (argc==1)
	{
	
		do_ls(".");
		return (0);
	}
	else {
		int  i=0;
	 	while (++i<argc)
	 	{
	 		printf ("DIrectory listing of %s :\n",argv[i]);
	 		do_ls(argv[i]);
	 	
	 	}
	 	return  0;
    }
}

void  do_ls(char* dir)
{
	struct  dirent *entry ;

    DIR* dp=opendir(dir);  
  	if  (dp==NULL)
  	{
	  	fprintf(stderr,"Cant opent  directory : %s\n",dir);
	  	return;
  	}
    errno=0;  
    while (1){
        entry =readdir(dp);
        if (entry==NULL && errno !=0){
            perror("readDir");
            return ;
        }
        else if (entry==NULL && errno ==0){ 
            return ;
        }
        if (entry->d_name[0]=='.'   )
		continue;
        printf("%s\n",entry->d_name);
    }
    closedir(dp);
}
